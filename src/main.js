import Vue from 'vue';
import Vuex from 'vuex';
import App from './App';
import 'whatwg-fetch';

Vue.use(Vuex);
Vue.config.productionTip = false;



// store
const store = new Vuex.Store({

    // state
    state: {
        'users': []
    },

    // mutations
    mutations: {

        // push users
        pushUsers(state, arr) {
            state.users = arr;
        }
    },

    // actions
    actions: {

        // push user
        pushUser({commit, state}, usr) {

            // if usr is array
            let newArr = [];
            if (usr.constructor === Array) {
                usr.forEach(function(item) {
                    newArr.push(constructUser(item, true));
                });
            } else {
                newArr.push(constructUser(usr));
            }
            const concatArr = [...state.users, ...newArr];
            commit('pushUsers', concatArr);

            // construct user
            function constructUser(obj, isRandom) {
                let newUser = {};

                // id
                newUser['id'] = generateID();

                // lastname
                newUser['lastname'] = isRandom ? obj.name.last : obj.lastname;

                // firstname
                newUser['firstname'] = isRandom ? obj.name.first : obj.firstname;

                // username
                newUser['username'] = isRandom ? obj.login.username : obj.username;

                // phone
                newUser['phone'] = obj.phone;

                // cell
                newUser['cell'] = obj.cell;

                // email
                newUser['email'] = obj.email;

                // location
                newUser['location'] = isRandom ? obj.location.city : obj.location;

                // address
                newUser['address'] = isRandom ? obj.location.street : obj.address;

                // postcode
                newUser['postcode'] = isRandom ? obj.location.postcode : obj.postcode;

                // registered
                newUser['registered'] = isRandom ? fixDate(obj.registered.date) : newDate();

                // birthday
                newUser['birthday'] = isRandom ? fixDate(obj.dob.date) : obj.birthday;

                // gender
                newUser['gender'] = obj.gender;

                // picture
                newUser['picture'] = isRandom ? obj.picture.large : obj.picture;

                // picture small
                newUser['picture_small'] = isRandom ? obj.picture.thumbnail : obj.picture;

                return newUser;
            }

            // fix date
            function fixDate(str) {
                let y = str.slice(0, 4),
                    m = str.slice(5, 7),
                    d = str.slice(8, 10);
                return m + '/' + d + '/' + y;
            }

            // new date
            function newDate() {
                let now = new Date().toISOString();
                return fixDate(now);
            }

            // generate ID
            function generateID() {
                return '_' + Math.random().toString(36).substr(2, 9);
            };
        },

        // remove user
        removeUser({commit, state}, userId) {
            let filterArr = state.users.filter( user => user.id !== userId );
            commit('pushUsers', filterArr);
        },
    }
});




// app
new Vue({
    el: '#app',
    template: '<App/>',
    components: { App },

    // created
    created: function () {
        this.getRandomUsers(10);
    },

    // methods
    methods: {

        // get random users
        getRandomUsers: function (count) {
            fetch('https://randomuser.me/api/?results=' + count)
                .then( function( response ) {
                    return response.json();
                }).then( function( data ) {
                    store.dispatch('pushUser', data.results);
                });
        },
    },
    store
});


